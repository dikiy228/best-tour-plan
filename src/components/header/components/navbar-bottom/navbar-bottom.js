import React from "react";
import "./navbar-bottom.scss";

const NavbarBottom = () => {
  return (
    <div className="navbarbottom">
      <div className="container">
        <nav className="navbarbottom__menu">
          <ul className="navbarbottom__items">
            <li className="navbarbottom__item"><a className="navbarbottom__link">All Deals</a></li>
            <li className="navbarbottom__item"><a className="navbarbottom__link">Hotels</a></li>
            <li className="navbarbottom__item"><a className="navbarbottom__link">Activities</a></li>
            <li className="navbarbottom__item"><a className="navbarbottom__link">Hotel Day Packages</a></li>
            <li className="navbarbottom__item"><a className="navbarbottom__link">Restaurants</a></li>
            <li className="navbarbottom__item"><a className="navbarbottom__link">Events</a></li>
            <li className="navbarbottom__item"><a className="navbarbottom__link">Rodrigues</a></li>
            <li className="navbarbottom__first"></li>
            <li className="navbarbottom__second"></li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default NavbarBottom;
